package com.example.twoactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button btnsimpan;
    private EditText namalengkap;
    private EditText namapanggilan;
    private EditText umur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnsimpan = (Button) findViewById(R.id.btnsimpan);
        namalengkap = (EditText) findViewById(R.id.namalengkap);
        namapanggilan = (EditText) findViewById(R.id.namapanggilan);
        umur = (EditText) findViewById(R.id.umur);

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                intent.putExtra("namalengkap", namalengkap.getText().toString());
                intent.putExtra("namapanggilan", namapanggilan.getText().toString());
                intent.putExtra("umur", Integer.parseInt(umur.getText().toString()));
                startActivity(intent);
            }
        });
    }
}