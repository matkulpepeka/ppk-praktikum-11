package com.example.twoactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {
    private String namalengkap, namapanggilan;
    private Integer umur;
    private TextView namalengkapView, namapanggilanView, umurView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Bundle extras = getIntent().getExtras();
        namalengkap = extras.getString("namalengkap");
        namapanggilan = extras.getString("namapanggilan");
        umur = extras.getInt("umur");

        namalengkapView = (TextView) findViewById(R.id.namalengkapView);
        namapanggilanView = (TextView) findViewById(R.id.namapanggilanView);
        umurView = (TextView) findViewById(R.id.umurView);

        namalengkapView.setText(namalengkap);
        namapanggilanView.setText(namapanggilan);
        umurView.setText(umur.toString());

    }
}